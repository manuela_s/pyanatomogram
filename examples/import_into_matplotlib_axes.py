import matplotlib.pyplot
import pandas
import pyanatomogram

data = pandas.Series({"heart": 0, "lung": 2, "brain": 3, "colon": 1, "liver": 4})

fig, axes = matplotlib.pyplot.subplots(nrows=1, ncols=2)

data.plot(kind="barh", ax=axes[0])
axes[0].set_xlabel("Relative expression")
axes[0].set_ylabel("Organ")

cmap = matplotlib.cm.Reds
norm = matplotlib.colors.Normalize()
norm.autoscale(data)
anatomogram = pyanatomogram.Anatomogram("gallus_gallus")
anatomogram.highlight_tissues(data.to_dict(), cmap=cmap, norm=norm)
anatomogram.to_matplotlib(ax=axes[1])

matplotlib.pyplot.colorbar(
    matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap),
    ax=axes[1],
    orientation="horizontal",
    label="Relative expression",
)
