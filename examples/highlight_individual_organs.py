import pyanatomogram

anatomogram = pyanatomogram.Anatomogram("gallus_gallus")
anatomogram.set_tissue_style("heart", fill="red")
anatomogram.set_tissue_style("lung", fill="blue")
anatomogram.to_matplotlib()
