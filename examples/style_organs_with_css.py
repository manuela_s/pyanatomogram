import pyanatomogram

anatomogram = pyanatomogram.Anatomogram("gallus_gallus")
anatomogram.set_tissue_style(
    "heart",
    **{"fill": "red", "stroke": "black", "stroke-width": 1, "fill-opacity": 0.8}
)
anatomogram.set_tissue_style(
    "lung",
    **{
        "fill": "cornflowerblue",
        "stroke": "darkblue",
        "stroke-width": 1,
        "fill-opacity": 0.5,
    }
)
anatomogram.to_matplotlib()
