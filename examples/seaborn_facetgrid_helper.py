import pandas
import pyanatomogram
import seaborn
import matplotlib

data = (
    pandas.DataFrame(
        {
            "Condition1": {"heart": 0, "lung": 2, "brain": 3, "colon": 1, "liver": 4},
            "Condition2": {"heart": 3, "lung": 4, "brain": 2, "colon": 2, "liver": 3},
            "Condition3": {"heart": 7, "lung": 9, "brain": 1, "colon": 7, "liver": 2},
        }
    )
    .stack()
    .rename_axis(["organ", "condition"])
    .to_frame("expression")
)

g = seaborn.FacetGrid(data.reset_index(), col="condition")
cmap = matplotlib.cm.Reds
norm = matplotlib.colors.Normalize()
norm.autoscale(data)
g.map(
    pyanatomogram.facetgrid_helper,
    "organ",
    "expression",
    name="gallus_gallus",
    cmap=cmap,
    norm=norm,
)
g.fig.colorbar(
    matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap), label="Relative expression"
)
