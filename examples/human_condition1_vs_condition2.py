import pyanatomogram
import matplotlib.pyplot
import numpy

numpy.random.seed(2)
fig, axes = matplotlib.pyplot.subplots(
    nrows=1,
    ncols=2,
    gridspec_kw=dict(bottom=0.15, top=0.95, left=0, right=1, wspace=0),
    figsize=(4, 7),
)

anatomogram = pyanatomogram.Anatomogram("homo_sapiens.male")
organs = anatomogram.get_tissue_names()
left = numpy.random.random(len(organs))
right = left * 0.5 + numpy.random.random(len(organs)) * 0.5
norm = matplotlib.colors.Normalize(vmin=0, vmax=1)

anatomogram.highlight_tissues(dict(zip(organs, left)), cmap="RdYlBu_r", norm=norm)
anatomogram.to_matplotlib(axes[0])
axes[0].set_title("Condition 1")

anatomogram.highlight_tissues(dict(zip(organs, right)), cmap="RdYlBu_r", norm=norm)
anatomogram.to_matplotlib(axes[1])
axes[1].set_title("Condition 2")

cax = matplotlib.pyplot.axes([0.25, 0.1, 0.5, 0.01])
fig.colorbar(
    matplotlib.cm.ScalarMappable(norm=norm, cmap="RdYlBu_r"),
    cax=cax,
    orientation="horizontal",
    label="Relative expression",
)

matplotlib.pyplot.tight_layout()
