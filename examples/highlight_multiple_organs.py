import pyanatomogram

anatomogram = pyanatomogram.Anatomogram("gallus_gallus")
anatomogram.highlight_tissues(
    {"heart": 0, "lung": 2, "brain": 3, "colon": 1, "liver": 4}, cmap="Reds"
)
anatomogram.to_matplotlib()
